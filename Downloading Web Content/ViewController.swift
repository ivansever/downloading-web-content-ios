//
//  ViewController.swift
//  Downloading Web Content
//
//  Created by Ivan Sever on 03/05/17.
//  Copyright © 2017 Ivan Sever. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
        let url = URL(string: "https://www.stackoverflow.com")!
        
        //  Load website data to url
        //webview.loadRequest(URLRequest(url: url))
        
        // load HTML String
        //webview.loadHTMLString("<h1>Just Heading</h1>", baseURL: nil)
        
        */
        
        //  downloads all html code in console        
        if let url = URL(string: "https://www.stackoverflow.com") {
            let request = NSMutableURLRequest(url: url)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if error != nil {
                    print(error as Any)
                } else {
                    if let unwrappedData = data {
                        let dataString = NSString(data: unwrappedData, encoding: String.Encoding.utf8.rawValue)
                        print(dataString as Any)

                    }
                }
            }
            task.resume()
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

